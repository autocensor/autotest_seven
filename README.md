AutoCensor平台部署
windows 环境下部署
0. 克隆项目
git clone https://gitee.com/oldboyedu/autotest_six.git
1. 安装 python 3 环境

2. 部署自然语言模型
2.1 解压压缩包

2.2 安装 python 依赖包

pip install tensorflow==1.14.0  -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install bert-serving-server==1.9.1 -i https://pypi.tuna.tsinghua.edu.cn/simple

2.3 启动模型
// 当前目录切换至模型文件夹目录后执行
bert-serving-start -model_dir ./chinese_L-12_H-768_A-12/ -num_worker=1

3. 部署 Mongodb 数据库

4. 设置系统环境变量
AUTOTEST_PLATFORM_ENV=production
AUTOTEST_PLATFORM_NLP_SERVER_HOST=127.0.0.1
AUTOTEST_PLATFORM_MONGO_HOST=${MONGO_HOST}
AUTOTEST_PLATFORM_MONGO_PORT=${MONGO_PORT}
AUTOTEST_PLATFORM_MONGO_USERNAME=${USERNAME}
AUTOTEST_PLATFORM_MONGO_PASSWORD=${PASSWORD}
AUTOTEST_PLATFORM_MONGO_DEFAULT_DBNAME=taisite
其中 AUTOTEST_PLATFORM_ENV 默认为 production （必填）

AUTOTEST_PLATFORM_MONGO_HOST和 AUTOTEST_PLATFORM_MONGO_PORT 分别表示数据库的地址和端口（必填）

AUTOTEST_PLATFORM_MONGO_USERNAME和 AUTOTEST_PLATFORM_MONGO_PASSWORD 分别表示数据库的帐号密码（若无可不填）

AUTOTEST_PLATFORM_NLP_SERVER_HOST（自然语言模型服务）默认为本机启动 （非必填）

AUTOTEST_PLATFORM_MONGO_DEFAULT_DBNAME 为默认的数据表名（必填）

设置完成后可通过下列命令进行测试（CMD切换至项目根目录下）

python ./backend/config.py
若配置成功则可看见输入的配置数据

5. 打包前端 dist 文件 （这一步我已为你们做好，若不需二次开发可跳过）
5.1 安装 Vue 环境，下载 node.js 并配置环境，下载 npm 包管理器

5.2 cmd 进入 frontend 目录下，配置 cnpm :

npm install -g cnpm --registry=https://registry.npm.taobao.org
5.3 执行安装依赖包命令:

cnpm install
5.4 执行打包命令:

cnpm run build
若成功打包则会在项目根目录下生成 dist 文件夹。

6. 启动后端
// 安装依赖包 (切换至项目根目录下执行)
pip install -r ./backend/requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple

// 启动后端 ( 默认5050端口 )
python ./backend/run.py

// 创建平台管理员帐号密码
python ./backend/createAdminUser.py
7. 访问项目
现在就可以访问 http://127.0.0.1:5050/#/login 使用创建的管理员帐号密码进行登录。

